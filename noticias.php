<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">
    <link rel="stylesheet" href="css/noticias.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI - Notícias</title>
</head>
<body id="pagina-noticias">
    <?php
        include 'includes/menu.php';
    ?>

    <div class="titulo-destaque">
        <div class="container">
            <div class="linha">
                <h3>Notícias</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="linha">
            <div class="doze colunas">
                <ul>
                    <li>
                        <a href="http://www.inpe.br/noticias/noticia.php?Cod_Noticia=4733" target="_blank">28-03-2018 - INPE - Projeto ensina como agir em inundações e eventos extremos</a>
                    </li>
                    <li>
                        <a href="http://www.mctic.gov.br/mctic/opencms/salaImprensa/noticias/arquivos/2018/01/Com_base_no_SOS_Chuva_cientistas_do_Inpe_criam_aplicativo_para_agrometeorologia_de_precisao.html" target="_blank">03-01-2018 - MCTIC - Cientistas do Inpe criam aplicativo com previsão imediata de chuvas para agricultores</a>
                    </li>
                    <li>
                        <a href="http://www.cocen.unicamp.br/noticias/artigo/260/cepagri-realiza-curso-de-capacitacao-para-o-aplicativo-sos-chuva" target="_blank">29-11-2017 - CEPAGRI Meteorologia - Cepagri realiza curso de capacitação para o aplicativo SOS Chuva</a>
                    </li>
                    <li>
                        <a href="http://www.esalq.usp.br/boletim/chuva-no-radar" target="_blank">29-11-2017 - Boletim ESALQ net - Chuva no radar</a>
                    </li>
                    <li>
                        <a href="https://g1.globo.com/sp/campinas-regiao/noticia/cepagri-da-unicamp-lanca-balao-meteorologico-para-antecipar-temporais.ghtml" target="_blank">28-11-2017 - Jornal da EPTV - Cepagri da Unicamp lança balão meteorológico para antecipar temporais</a>
                    </li>
                    <li>
                        <a href="http://revistapesquisa.fapesp.br/2017/10/25/telas-do-aplicativo-sos-chuva-do-inpe/" target="_blank">25-10-2017 - Pesquisa FAPESP - Chuva da hora no celular</a>
                    </li>
                    <li>
                        <a href="http://radios.ebc.com.br/tarde-nacional/2017/08/satelite-considerado-o-melhor-do-mundo-e-utilizado-pelo-inpe-no-brasil" target="_blank">02-08-2017 - EBC - Satélite considerado o melhor do mundo é utilizado pelo Inpe</a>
                    </li>
                    <li>
                        <a href="http://www.saoborja.rs.gov.br/index.php/ultimas-noticias/116-inpe" target="_blank">27-07-2017 - Prefeitura de São Borja - INPE pretende implantar radar móvel em São Borja</a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/watch?v=7bD2K8GJiNA&feature=youtu.be" target="_blank">23-06-2017 - Fóruns Permanentes - Eventos Meteorológicos Extremos</a>
                    </li>
                    <li>
                        <a href="http://agencia.fapesp.br/modelo_do_inpe_permite_prever_a_ocorrencia_de_raios_durante_as_tempestades/24662/" target="_blank">24-01-2017 - Modelo do INPE permite prever a ocorrência de raios durante as tempestades</a>
                    </li>
                    <li>
                        <a href="http://www.crea-rs.org.br/site/revista_pageflip/119/magazine-sample/index.html#page/36" target="_blank">04-01-2017 - Revista CREA – Aplicativo de previsão imediata para chuva é testado em São Paulo</a>
                    </li>
                    <li>
                        <a href="http://noticias.r7.com/fala-brasil/videos/aplicativo-criado-por-instituo-meteorologico-informa-chuvas-fortes-e-possibilidade-de-raios-27012017" target="_blank">27-01-2017 - R7 – Aplicativo criado por instituo meteorológico informa chuvas fortes e possibilidade de raios</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <?php
        include 'includes/rodape.php';
    ?>

    <script>
        $( document ).ready(function() {
            $('.nav.menu a:nth-of-type(3)').addClass('ativo');
        });
    </script>
</body>