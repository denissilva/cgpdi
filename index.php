<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI</title>

    <style>
        #item-header1{
            background-image: url('img/projetos/slide_mywater.jpg');
        }

        #item-header2{
            background-image: url('img/projetos/slide_mywater2.jpg');
        }

        #item-header3{
            background-image: url('img/projetos/slide_nraios.jpg');
        }

        #item-header4{
            background-image: url('img/projetos/slide_nraios2.jpg');
        }
    </style>
</head>
<body>
    <?php
        include 'includes/menu.php';
    ?>
    
    <header class="header">
        <div class="owl-carousel owl-theme">
            <div class="item-header" id="item-header1">
                <h2>Projeto My Water</h2>
            </div>
            <div class="item-header" id="item-header2">
                <h2>Projeto My Water</h2>
            </div>
            <div class="item-header" id="item-header3">
                <h2>Núcleo de Monitoramento de Descargas Elétricas</h2>
            </div>
            <div class="item-header" id="item-header4">
                <h2>Núcleo de Monitoramento de Descargas Elétricas</h2>
            </div>
        </div>        
    </header>

    <div id="boxes">
        <div class="container">
            <div class="linha secao">
                <!-- <div class="quatro colunas">
                    <h3 class="box titulo">Pesquisadores</h3>
                    <div class="limitador">
                        <div class="box cabecalho pesquisadores"></div>

                        <div class="box conteudo">
                            <p class="box paragrafo">
                                Acesse sua planilha e acompanhe sua rubrica
                            </p>

                            <a href="/cgpdi_admin" target="_blank" class="botao botao destacado">Acesse</a>
                        </div>
                    </div>
                </div> -->
                <div class="seis colunas plano">
                    <h3 class="box titulo">Projetos</h3>
                    <div class="limitador">
                        <div class="box cabecalho projetos"></div>
                        <div class="box conteudo">
                            <p class="box paragrafo">
                                Conheça nossos cases e parceiros
                            </p>

                            <a href="projetos.php" class="botao botao destacado">Acesse</a>
                        </div>
                    </div>
                </div>
                <div class="seis colunas plano">
                    <h3 class="box titulo">Licitações</h3>
                    <div class="limitador">
                        <div class="box cabecalho licitacoes"></div>
                        <div class="box conteudo">
                            <p class="box paragrafo">
                                Pregões e licitações
                            </p>

                            <a href="licitacoes.php" class="botao botao destacado">Acesse</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="quem-somos" id="quem-somos">
        <div class="container">
            <div class="linha">
                <div class="doze colunas">
                    <h2>Quem Somos</h2>
                </div>
                <div class="seis colunas">
                    <p>O CGPDI atua em parceria com instituições e programas de pesquisa e de ensino, em cooperação técnico-científica, buscando o desenvolvimento de atividades de pesquisa científica no país, P&D e a formação de recursos humanos qualificados.</p>

                    <p>A associação também oferece facilidades para o desenvolvimento de programas de pesquisa, subprojetos, realização de eventos de caráter científico, tecnológico, treinamento, capacitação e educação.</p>

                    <p>O CGPDI também celebra parcerias, firmando contratos de consultoria, bem como convênios de cooperação técnico-científica, com instituições nacionais ou internacionais, públicas ou privadas, atendendo interesse das instituições parceiras.</p>

                    <a href="quem-somos.php" class="botao pull-right">Saiba Mais</a>
                </div>
                <div class="seis colunas">
                    <img src="img/quem-somos.jpg" alt="" class="u-width-100">
                </div>
            </div>
        </div>
    </div>

    <div id="noticias-eventos">
        <div class="container">
            <div class="linha secao">
                <div class="doze colunas">
                    <h2>Notícias</h2>
                </div>

                <div class="seis colunas">
                    <div class="noticia noticia-destaque">
                        <div class="noticia-titulo">
                            INPE será o centro de dados da missão europeia Copernicus na America Latina
                        </div>
                    </div>
                </div>

                <div class="seis colunas">
                    <div class="linha">
                        <div class="seis colunas">
                            <div class="noticia noticia-menor noticia1">
                                <div class="noticia-titulo">
                                    Aberta as inscrições para o curso de introdução à astronomia e astrofísica
                                </div>
                            </div>
                        </div>

                        <div class="seis colunas">
                            <div class="noticia noticia-menor noticia2">
                                <div class="noticia-titulo">
                                    Software do INPE para monitoramento ganha nova versão
                                </div>
                            </div>
                        </div>

                        <div class="seis colunas">
                            <div class="noticia noticia-menor noticia3">
                                <div class="noticia-titulo">
                                    1º ClubeDesign será apresentado durante evento da Nasa em São José dos Campos
                                </div>
                            </div>
                        </div>

                        <div class="seis colunas">
                            <div class="noticia noticia-menor noticia4">
                                <div class="noticia-titulo">
                                    Incêndios florestais dominam as emissões de carbono durante secas na Amazônia
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="depoimentos">
        <div class="container">
            <div class="linha secao">
                <div class="doze colunas">
                    <h2>Depoimentos</h2>

                    <div class="owl-carousel owl-theme">
                        <div class="item-depoimento" id="item-depoimento1">
                            <p class="depoimento-nome">
                                Paulo Eduardo Artaxo
                            </p>
                            <p class="depoimento-texto">
                                "Com todas as dificuldades burocráticas de gerenciamento financeiro de projetos de pesquisas, encontrar o CGPDI (ex-APLBA) foi algo realmente importante. Facilitou muito o recebimento de recursos financeiros de agências de fomento estrangeiras, e permitiu gastar os recursos com eficiência e segurança de correta contabilidade."
                            </p>
                            <p class="depoimento-instituicao">
                                Instituto de Física, Universidade de São Paulo
                            </p>
                        </div>

                        <div class="item-depoimento" id="item-depoimento2">
                            <p class="depoimento-nome">
                                Luiz Augusto Toledo
                            </p>
                            <p class="depoimento-texto">
                                "O CGPDI tem apoiado a pesquisa no Brasil desde a sua criação no ano 2000, na época do Experimento LBA. Diversos projetos, sejam de pesquisa básica ou aplicada, foram cuidadosamente gerenciados e apoiados pelo CGPDI. Em particular, destaco o apoio em cooperações internacionais de experimentos científicos, muitas vezes complexos, que necessitam de um grande esforço administrativo tanto na gestão de recursos humanos como na aquisição ou na importação temporária de instrumentos científicos."
                            </p>
                            <p class="depoimento-instituicao">
                                Centro de Previsão de Tempo e Estudos Climáticos - Instituto Nacional de Pesquisas Espaciais
                            </p>
                        </div>

                        <div class="item-depoimento" id="item-depoimento3">
                            <p class="depoimento-nome">
                                Carlos Frederico
                            </p>
                            <p class="depoimento-texto">
                                "Os serviços prestados pelo CGPDI são de altíssima qualidade e refletem a competência da equipe administrativa da instituição. A eficiente administração de grandes projetos feita pela CGPDI nos dá uma segurança muito grande, pois, como coordenadores, precisamos nos dedicar integralmente à execução dos projetos deixando a parte administrativa em boas mãos."
                            </p>
                            <p class="depoimento-instituicao">
                                Centro Nacional de Monitoramento de Alertas e Desastres Naturais
                            </p>
                        </div>

                        <div class="item-depoimento" id="item-depoimento4">
                            <p class="depoimento-nome">
                                Reinaldo Silveira
                            </p>
                            <p class="depoimento-texto">
                                "O Centro de  Gestão de Pesquisa, Desenvolvimento e Inovação – CGPDI tem auxiliado a execução de projetos sob minha responsabilidade, em particular os Projetos SIMPAT e SINAL-SOS da FINEP, facilitando muito a gestão administrativa destes projetos. Os funcionários do CGPDI são muito competentes e sempre prontos a auxiliar e resolver dificuldades como processos licitatórios, fiscais e jurídicos,  pertinentes  às aquisições de bens, serviços, pagamentos e viagens, destacando-se alta capacitação do Centro nestes assuntos. Recomendo-o com segurança, inclusive para projetos nos quais a administração requer experiência e eficiência para atingir os resultados."
                            </p>
                            <p class="depoimento-instituicao">
                                Instituto Tecnológico SIMEPAR
                            </p>
                        </div>

                        <div class="item-depoimento" id="item-depoimento5">
                            <p class="depoimento-nome">
                                Waldênio Gambi
                            </p>
                            <p class="depoimento-texto">
                                "Tenho trabalhado com o CGPDI há vários anos, período em que o CGPDI administrou três projetos desenvolvidos sob a minha coordenação. Sempre fiquei muito satisfeito com o atendimento gentil, eficiente e muito profissional da equipe. Muitas vezes o apoio da equipe se revelou essencial para esclarecer pontos nebulosos frequentemente encontrados na administração de projetos."
                            </p>
                            <p class="depoimento-instituicao">
                                Centro de Previsão de Tempo e Estudos Climáticos - Instituto Nacional de Pesquisas Espaciais
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        include 'includes/rodape.php';
    ?>

    <script src="js/owl.carousel.min.js"></script>
    <script>
        $('.owl-carousel').owlCarousel({
            items: 1,
            nav: true,
            loop: true,
            navText: [
                "<i class='fa fa-angle-left'></i>",
                "<i class='fa fa-angle-right'></i>"
            ],
            dots: true,
            autoplay: true
        })
    </script>
</body>
</html>