<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI - Diretoria</title>
</head>
<body>
    <?php
        include 'includes/menu.php';
    ?>

    <div class="titulo-destaque">
        <div class="container">
            <div class="linha">
                <h3>Diretoria</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="linha secao">
            <div class="seis colunas">
                <p class="u-align-center">
                    <b>Dirceu Luis Herdies</b><br />
                    Presidente
                </p>
            </div>
            <div class="seis colunas">
                <p class="u-align-center">
                    <b>Waldênio Gambi de Almeida</b><br />
                    Vice-Presidente
                </p>
            </div>
        </div>
    </div>

    <?php
        include 'includes/rodape.php';
    ?>
</body>