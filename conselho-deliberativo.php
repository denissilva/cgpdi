<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">
    <link rel="stylesheet" href="css/equipe.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI - Conselho Deliberativo</title>
</head>
<body>
    <?php
        include 'includes/menu.php';
    ?>

    <div class="titulo-destaque">
        <div class="container">
            <div class="linha">
                <h3>Conselho Deliberativo</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="linha secao">
            <div class="seis colunas">
                <div class="equipe">
                    <b>Dr. Dirceu Luis Herdies</b><br />
                    Presidente
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe">
                    <b>Dr. Paulo Eduardo Artaxo Netto</b><br />
                    Instituto de Física - IF/USP
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe">
                    <b>Dra. Maria Assunção Faus da Silva Dias</b><br />
                    Instituto Atmosférico e Geofísico - IAG/USP
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe">
                    <b>Dr. Antonio Donato Nobre</b><br />
                    Instituto Nacional de Pesquisas da Amazônia - INPA/INPE
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe">
                    <b>Dra. Júlia Clarinda Paiva Cohen</b><br />
                    Universidade Federal do Pará - UFPA
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe">
                    <b>Dra. Thelma Krug</b><br />
                    Instituto Nacional de Pesquisas Espaciais - INPE
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe">
                    <b>Dr. Humberto Ribeiro da Rocha</b><br />
                    Instituto de Astronomia, Geofísica e Ciências Atmosféricas - IAG/USP
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe">
                    <b>Dr. Pedro Leite Silva Dias</b><br />
                    Instituto Atmosférico e Geofísico - IAG/USP
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe"> 
                    <b>Dr. Waldênio Gambi de Almeida</b><br />
                    Centro de Previsão do Tempo e Estudos Climáticos - CPTEC/INPE
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe"> 
                    <b>Dr. José Antonio Marengo Orsini</b><br />
                    CEMADEN
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe"> 
                    <b>Dr. Antonio Ocimar Manzi</b><br />
                    Centro de Previsão do Tempo e Estudos Climáticos - CPTEC/INPE
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe"> 
                    <b>Dr. Jean Pierre Henry Balbaud Ometto</b><br />
                    Centro de Ciência do Sistema Terrestre - CCST/INPE
                </div>
            </div>
        </div>
    </div>

    <?php
        include 'includes/rodape.php';
    ?>

    <script>
        $( document ).ready(function() {
            $('.nav.menu .conteudoMenu:nth-of-type(1) a:nth-of-type(1)').addClass('ativo');
            $('.nav.menu .conteudoMenu:nth-of-type(2) a:nth-of-type(3)').addClass('ativo');
        });
    </script>
</body>