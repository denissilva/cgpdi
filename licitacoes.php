<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">
    <link rel="stylesheet" href="css/noticias.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI - Lcitações</title>
</head>
<body id="pagina-noticias">
    <?php
        include 'includes/menu.php';
    ?>

    <div class="titulo-destaque">
        <div class="container">
            <div class="linha">
                <h3>Licitações</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="linha">
            <div class="doze colunas">
                <ul id="list-licitacao"></ul>
            </div>
        </div>
    </div>

     <script src="js/jquery.min.js"></script>
    <script>
        $.getJSON('/cgpdi_admin/controller/licitacao/lista.php', function(list){
            for (i in list) {
                tipo = "Outro";
                
                if (list[i].tipo == "TP")
                    tipo = "Tomada de Preço";
                else if (list[i].tipo == "PE")
                    tipo = "Pregão Eletrônico";
                else if (list[i].tipo == "PP")
                    tipo = "Pregão Presencial";
                else if (list[i].tipo == "LI")
                    tipo = "Licitação";
                
                
                sub = "<ul>";
                arqs = list[i].arquivos.split(";");
                for(x in arqs){
                    
                    niveis = arqs[x].split("/");
                    url = "/cgpdi_admin/upload/"+niveis[niveis.length-3]+"/"+niveis[niveis.length-2]+"/"+niveis[niveis.length-1];
                    sub += "<li><a href='"+url+"' target='_blank'>Anexo - "+x+"<a></li>";    
                }
                sub += "</ul>";
                
                $("#list-licitacao").append('<li><strong>'+tipo+' N.º '+list[i].codigo+' - '+list[i].titulo+'</strong>'+sub+'</li>') 
            }
        });
        
    </script>

    <?php
        include 'includes/rodape.php';
    ?>
</body>