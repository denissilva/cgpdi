<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI - Quem Somos</title>
</head>
<body>
    <?php
        include 'includes/menu.php';
    ?>

    <div class="titulo-destaque">
        <div class="container">
            <div class="linha">
                <h3>Portal da Transparência</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="linha">
            <div class="doze colunas">
                <h4>Projetos e Documentos</h4>
                <div class="linha">
                    <div class="quatro colunas">
                        <input type="text" class="u-width-100" placeholder="Pesquisa...">
                    </div>
                </div>
                <ul id="projetos-documentos"></ul>
            </div>
        </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script>
        $(function(){
            $('input[type="text"]').keyup(function(){
                
                var searchText = $(this).val();
                
                $('ul#projetos-documentos > li').each(function(){
                    
                    var currentLiText = $(this).text(),
                        showCurrentLi = currentLiText.toLowerCase().indexOf(searchText) !== -1 || currentLiText.indexOf(searchText) !== -1;
                    
                    $(this).toggle(showCurrentLi);
                    
                });     
            });
        });

         $.getJSON('/cgpdi_admin/controller/projeto/lista.php', function (projects) {
            li = '';

            for (p in projects){
                li += '<li class="collapse" id="'+projects[p].id+'">';
                    li += '<span class="collapse-title">'
                        li += '<i class="fas fa-plus-square"></i>';
                        li += '<label>'+projects[p].nome+'</label>';
                    li += '</span>'
                    li += '<div class="collapse-content" id="collapse_'+projects[p].id+'">';
                        li += '<div class="linha">';
                            li += '<div class="seis colunas">';
                                li += '<span class="u-text-bold">Data Início:</span> '+projects[p].data_inicio;
                            li += '</div>';
                            li += '<div class="seis colunas">';
                                li += '<span class="u-text-bold">Data Término:</span> '+projects[p].data_fim;
                            li += '</div>';
                            li += '<div class="doze colunas">';
                                li += '<span class="u-text-bold">Descrição:</span> '+projects[p].descricao;
                            li += '</div>';
                            li += '<div class="doze colunas">';
                                li += '<span class="u-text-bold">Arquivos:</span>';
                                    li += '<ul id="files_'+projects[p].id+'"></ul>';
                            li += '</div>';
                        li += '</div>';
                    li += '</div>';
                li += '</li>';
            }
            $("#projetos-documentos").html(li);
         });

         $('body').on('click', '.collapse-title', function (){
            id = $(this).parent().attr('id');
            fi = '';

            icon = $(this).find('svg');

            if($("#collapse_"+id).is(":visible")){
                $("#collapse_"+id).slideUp();
                $(icon).removeClass('fa-minus-square').addClass('fa-plus-square');
            }else{
                $(icon).removeClass('fa-plus-square').addClass('fa-minus-square');
                $.ajax({
                    url: '/cgpdi_admin/controller/publicacao/listaPublica.php',
                    type: 'GET',
                    data: {id_projeto: id},
                    async: false,
                    success: function(files) {
                        filesObj = JSON.parse(files);
                        if(filesObj.length == 0)
                            fi += '<li>Não existem documentos cadastrados para esse projeto.</li>'
                        else{
                            for (f in filesObj){
                                caminho = filesObj[f].caminho
                                nome = caminho.split("//");
                                
                                fi += '<li>';
                                fi += '<a href="'+caminho+'">';
                                fi += nome[nome.length-1]
                                fi += '</a>'
                                fi += '</li>';
                            }
                        }
                    }
                });

                $("#files_"+id).html(fi);
                $("#collapse_"+id).slideDown();
            }
         });
    </script>

    <?php
        include 'includes/rodape.php';
    ?>
</body>
</html>