<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI - Quem Somos</title>
</head>
<body>
    <?php
        include 'includes/menu.php';
    ?>

    <div class="titulo-destaque">
        <div class="container">
            <div class="linha">
                <h3>Quem Somos</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="linha">
            <div class="doze colunas">
                <h4>O que é o CGPDI?</h4>
                <p>Uma associação sem fins lucrativos, há mais de 15 anos contribuindo com o desenvolvimento científico do país, através da gestão de projetos de pesquisa e apoia atividades científicas e de desenvolvimento tecnológico e de inovação de interesse de instituições de pesquisa e ensino no país.</p>
                <!-- <h4>Documentos</h4> -->
                <ul>
                    <li>
                        <a href="pdf/ESTATUTO CGPDI Registrado 2012.pdf" target="_blank">Estatuto - 2012</a>
                    </li>
                    <li>
                        <a href="pdf/ATA ALTERAÇAO ESTATUTO APLBA 2012.pdf" target="_blank">Ata Alteração do Estatuto - 2012</a>
                    </li>
                </ul>
                
                <h4>Como atua?</h4>
                <p>Através da sua Secretaria Executiva, o CGPDI celebra parcerias firmando contratos de consultoria, bem como convênios de cooperação técnico-científica, com instituições nacionais e/ou internacionais, atendendo interesse das instituições parceiras. Em todos os casos, o CGPDI faz a gestão de recursos financeiros.</p>
                
                <h4>História</h4>
                <p>No ano 2000, pesquisadores líderes do Experimento de Grande Escala da Biosfera-Atmosfera na Amazônia – LBA criaram uma associação sem fins lucrativos com o objetivo de criar facilidades para o desenvolvimento das pesquisas do Experimento LBA realizadas em sítios de pesquisa localizados na Amazônia brasileira. </p>
                <p>Assim, a Associação dos Pesquisadores do Experimento de Grande Escala da Biosfera-Atmosfera na Amazônia – APLBA, desde então, dedicou-se à gestão de projetos de pesquisa do Experimento LBA.</p>
                <p>Com o sucesso e a facilidade com que a APLBA sempre atendeu seus parceiros vinculados ao LBA, a associação passou a despertar o interesse de outros líderes de pesquisa e ganhou a confiança de outras organizações nas áreas de meio ambiente e desenvolvimento tecnológico. A confiança conquistada ao longo dos anos, naturalmente, é fruto da seriedade e profissionalismo com que a associação sempre executou suas atividades de gestão.</p>
                <p>A partir do ano 2011, contudo, a APLBA constatou que precisava ampliar sua área de atuação para atender outros projetos de pesquisa, bem como mudar o nome da associação, de modo que pudesse firmar contratos com instituições financiadoras de outros projetos desenvolvidos fora da Amazônia.</p>
                <p>Foi assim que, em Agosto de 2012, através de uma Assembleia Geral, os associados, escolheram um novo nome para a associação, passando para Centro de Gestão de Pesquisa, Desenvolvimento e Inovação – CGPDI, instituído também sob a forma jurídica de ASSOCIAÇÃO, Pessoa Jurídica de Direito Privado, sem fins econômicos.</p>
                <p>Desde então, o CGPDI vem servindo e promovendo a integração da comunidade científica, gerindo projetos nacionais e internacionais e contribuindo significativamente para a produção de novos conhecimentos.</p>
            </div>
        </div>
    </div>

    <?php
        include 'includes/rodape.php';
    ?>

    <script>
        $( document ).ready(function() {
            $('.nav.menu .conteudoMenu:nth-of-type(1) a:nth-of-type(1)').addClass('ativo');
            $('.nav.menu .conteudoMenu:nth-of-type(2) a:nth-of-type(1)').addClass('ativo');
        });
    </script>
</body>