<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">
    <link rel="stylesheet" href="css/noticias.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI - Projetos</title>
</head>
<body id="pagina-noticias">
    <?php
        include 'includes/menu.php';
    ?>

    <div class="titulo-destaque">
        <div class="container">
            <div class="linha">
            <div class="doze colunas">
                    <h3>Instituições Parceiras e Respectivos Projetos</h3>
                </div>

                <!-- <div class="uma colunas">
                    <a href="pdf/CGPDI - Projetos.pdf" target="_blank"><img src="img/pdf.png" title="Download da lista de Projetos" width="50" height="50" style="margin-top: 20px;"></a>
                </div> -->
            </div>
        </div>
    </div>

    <div class="container">
        <div class="linha">
            <div class="doze colunas"><ul id="lista-projetos"></ul></div>
        </div>
    </div>

    <script src="js/jquery.min.js"></script>
    <script>
         $.getJSON('/cgpdi_admin/controller/projeto/lista.php?campo=data_fim', function (list) {
            $.getJSON('/cgpdi_admin/controller/instituicao/lista.php', function(instituicoes){
                for (i in list) {
                    
                    li = "<li>";
                    // li += "<strong>Instituições:</strong>";
                    li += "<div style='text-align: center'>";
                    insts = list[i].instituicao.split(";");
                    for(x in insts){
                        for(y in instituicoes){
                            if(insts[x] == instituicoes[y].id){
                                niveis = instituicoes[y].caminho.split("/");
                                url = "/cgpdi_admin/upload/"+niveis[niveis.length-2]+"/"+niveis[niveis.length-1];
                                li += "<img src='"+url+"' title='"+url+"'>";
                            }
                        }
                    }
                    li += "</div>";
                    li += "<a " + (list[i].link == "" ? "" : "href='" + list[i].link + "'") + " target='_blank'><h4>" + list[i].nome + "</h4></a>";
                    li += (list[i].sigla ? "<p><strong>" + list[i].sigla + "</strong></p>" : "")
                    li += "<div><strong>Período:</strong> De " + list[i].data_inicio + " à " + list[i].data_fim + "</div>"
                    li += "<p><strong>Descrição:</strong><br />" + list[i].descricao + "</p>";
                    li += "</li>";
                    $("#lista-projetos").append(li);
                }
            });
        });
    </script>
    <?php
        include 'includes/rodape.php';
    ?>
</body>