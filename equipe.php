<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">
    <link rel="stylesheet" href="css/equipe.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI - Equipe</title>
</head>
<body>
    <?php
        include 'includes/menu.php';
    ?>

    <div class="titulo-destaque">
        <div class="container">
            <div class="linha">
                <h3>Equipe</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="linha">
            <div class="doze colunas">
                <h3 class="equipe-titulo">Diretoria</h3>
            </div>
            <div class="seis colunas">
                <div class="equipe">
                    <img src="img/equipe/dirceu-herdies.png" alt="" width="100" height="100">
                    <b>Dirceu Luis Herdies</b><br />
                    Presidente
                </div>
            </div>
            <div class="seis colunas">
                <div class="equipe">
                    <img src="img/equipe/waldenio-gambi.png" alt="" width="100" height="100">
                    <b>Waldênio Gambi de Almeida</b><br />
                    Vice-Presidente
                </div>
            </div>
            <div class="doze colunas">
                <h3 class="equipe-titulo">Secretaria Executiva</h3>
            </div>
            <div class="seis colunas u-div-center">
                <div class="equipe">
                    <img src="img/equipe/yara-lopes.png" alt="" width="100" height="100">
                    <b>Yara Lopes Guedes Ferreira</b>
                </div>
            </div>

            <div class="doze colunas">
                <h3 class="equipe-titulo">Financeiro</h3>
            </div>

            <div class="seis colunas u-div-center">
                <div class="equipe">
                    <img src="img/equipe/financeiro.png" alt="" width="100" height="100">
                    <b>Ericka dos Santos B. Gonçalves</b><br />
                    <b>Alessandra Vieira Gomes</b><br />
                    <b>Erika P. de Oliveira Antunes</b>
                </div>
            </div>

            <div class="doze colunas">
                <h3 class="equipe-titulo">Patrimônio/Compras</h3>
            </div>

            <div class="seis colunas u-div-center">
                <div class="equipe">
                    <img src="img/equipe/adna-sabara.png" alt="" width="100" height="100">
                    <b>Adna Sabará Lemes</b>
                </div>
            </div>

            <div class="doze colunas">
                <h3 class="equipe-titulo">Setor de Pessoal</h3>
            </div>

            <div class="seis colunas u-div-center">
                <div class="equipe">
                    <img src="img/equipe/jessica-pinto.png" alt="" width="100" height="100">
                    <b>Jéssica C. Pinto de Almeida</b>
                </div>
            </div>
        </div>
    </div>

    <?php
        include 'includes/rodape.php';
    ?>

    <script>
        $( document ).ready(function() {
            $('.nav.menu .conteudoMenu:nth-of-type(1) a:nth-of-type(1)').addClass('ativo');
            $('.nav.menu .conteudoMenu:nth-of-type(2) a:nth-of-type(5)').addClass('ativo');
        });
    </script>
</body>