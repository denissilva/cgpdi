<?php
    header("Cache-Control: no-cache, must-revalidate");
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header("Content-Type: application/xml; charset=utf-8");

    // Destinatário
    $para = "denis1206@hotmail.com";

    // Assunto do e-mail
    $assunto = "Formulário de Contato do Site";

    // Campos do formulário de contato
    $nome = $_POST['nome'];
    $email = $_POST['email'];
    $mensagem = $_POST['mensagem'];

    // Monta o corpo da mensagem com os campos
    $corpo = "<html><head></head><body style='padding: 20px; border: 1px solid #000'>";
    $corpo .= "<div>";
    $corpo .= "<img src='http://pyata.cptec.inpe.br/sigmacast/img/bannerEmailCgpdi.jpg' style='display: block; width: 100%; margin-bottom: 20px;' />";
    $corpo .= "Olá $nome, recebemos sua mensagem e entraremos em contato dentro de 48hrs. <br/><br/>";
    $corpo .= "<b>Email:</b> $email <br /> <b>Mensagem:</b> $mensagem <br/><br/><br/><br/><br/>";
    $corpo .= "<b>Equipe CGPDI</b>";
    $corpo .= "</div>";
    $corpo .= "</body></html>";
    
    // Cabeçalho do e-mail
    $headers = "From: john@example.com\n";
    $headers .= "Reply-To: john@example.com\n";
    $headers .= "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=utf-8";

    clearstatcache();

    $result = mail($para, '=?UTF-8?B?'.base64_encode($assunto).'?=', $corpo, $headers);

    if($result){
        echo "E-mail enviado com sucesso!";
    }else{
        echo "Erro ao enviar o e-mail!" . $result;
    }