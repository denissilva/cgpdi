<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">
    <link rel="stylesheet" href="css/equipe.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI - Conselho Fiscal</title>
</head>
<body>
    <?php
        include 'includes/menu.php';
    ?>

    <div class="titulo-destaque">
        <div class="container">
            <div class="linha">
                <h3>Conselho Fiscal</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="linha secao">
            <div class="doze colunas">
                <ul>
                    <li>
                        Turíbio Gomes Soares Netto
                    </li>
                    <li>
                        Luis Gustavo G. de Gonçalves
                    </li>
                    <li>
                        Diego Oliveira Souza
                    </li>
                    <br /><br /><br /><br />
                </ul>
            </div>
        </div>
    </div>

    <?php
        include 'includes/rodape.php';
    ?>

    <script>
        $( document ).ready(function() {
            $('.nav.menu .conteudoMenu:nth-of-type(1) a:nth-of-type(1)').addClass('ativo');
            $('.nav.menu .conteudoMenu:nth-of-type(2) a:nth-of-type(4)').addClass('ativo');
        });
    </script>
</body>