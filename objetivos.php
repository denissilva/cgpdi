<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI - Objetivos</title>
</head>
<body>
    <?php
        include 'includes/menu.php';
    ?>

    <div class="titulo-destaque">
        <div class="container">
            <div class="linha">
                <h3>Objetivos</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="linha">
            <div class="doze colunas">
                <ol>
                    <li>Promover ações que facilitem e estreitem os laços de cooperação técnico-científica entre pesquisadores integrantes da Associação, e entre estes e as instituições públicas e privadas, nacionais e estrangeiras financiadoras de projetos de pesquisa e desenvolvimento nas suas áreas de atuação;</li>
                    <li>Conjugar esforços científicos com instituições de interesse em projetos comprometidos com as suas áreas de atuação, visando o desenvolvimento institucional, bem como complementando o esforço dessas instituições no que diz respeito à transferência de tecnologias, metodologias e serviços, e assessorando-as em temas específicos;</li>
                    <li>Interagir com o setor produtivo, governamental ou não, desenvolvendo ações que visem inovações científicas e tecnológicas de alto nível em prol do desenvolvimento sustentável do meio ambiente e do desenvolvimento tecnológico em geral, relacionados às suas áreas de atuação;</li>
                    <li>Prestar serviços de fornecimento e administração de mão-de-obra especializada, dentro das suas áreas de atuação;</li>
                    <li>Realizar trabalhos, executar e apoiar a execução de projetos e programas dentro das suas áreas de atuação, tornando-os exequíveis;</li>
                    <li>Promover, incentivar e apoiar estudos, atividades de pesquisa e desenvolvimento científico, tecnológico e industrial, nas áreas de alta tecnologia, dentro das suas áreas de atuação;</li>
                    <li>Estimular a formação, especialização e desenvolvimento profissional nas áreas de seu interesse, dentro das suas áreas de atuação, assim como complementar as atividades de capacitação e treinamento de pessoal levadas a efeito pelas instituições parceiras;</li>
                    <li>Colaborar com instituições de meio ambiente, meio ambiente marinho, saúde, química atmosférica, efeito estufa, recursos hídricos, meteorologia, clima, aplicações de imagens de satélites, sensoriamento remoto e desenvolvimento científico e tecnológico, propiciando-lhes a divulgação de resultados de pesquisas e patentes, edição de livros e artigos, participação em congressos, mostras, feiras e correlatos, e apoio a projetos de pesquisa e desenvolvimento científico, tecnológico e institucional;</li>
                    <li> Promover, incentivar e executar atividades que utilizem, direta ou indiretamente, técnicas, processos ou produtos provenientes ou decorrentes das atividades desenvolvidas pelos projetos de pesquisa, cujo objeto seja compatível com as áreas de atuação da associação;</li>
                    <li> Facilitar e apoiar o desenvolvimento científico e tecnológico do país, gerenciando recursos financeiros de apoio a projetos de pesquisa e tecnologia.
Sempre no âmbito de seus objetivos, poderá o CGPDI, expressamente,  firmar parcerias, convênios e prestar seus serviços a instituições, públicas ou privadas, nacionais ou estrangeiras, comprometidas com a pesquisa, a defesa do meio ambiente em geral, em defesa da saúde, e do desenvolvimento científico-tecnológico.</li>
                </ol>
            </div>
        </div>
    </div>

    <?php
        include 'includes/rodape.php';
    ?>

    <script>
        $( document ).ready(function() {
            $('.nav.menu .conteudoMenu:nth-of-type(1) a:nth-of-type(1)').addClass('ativo');
            $('.nav.menu .conteudoMenu:nth-of-type(2) a:nth-of-type(2)').addClass('ativo');
        });
    </script>
</body>