<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CGPDI</title>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/projeto.css">
    <link rel="stylesheet" href="css/contato.css">

    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <title>CGPDI - Contato</title>
</head>
<body>
    <?php
        include 'includes/menu.php';
    ?>

    <div class="titulo-destaque">
        <div class="container">
            <div class="linha">
                <h3>Contato</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="linha secao">
            <div class="doze colunas">
                <form method="post">
                    <div class="linha">
                        <div class="doze colunas">
                            <div id="alerta"></div>
                        </div>
                        <div class="seis colunas">
                            <label for="nome">Nome</label>
                            <input type="text" id="nome" name="nome" class="u-width-100" required>
                        </div>
                        <div class="seis colunas">
                            <label for="email">E-mail</label>
                            <input type="text" id="email" name="email" class="u-width-100" required>
                        </div>
                    </div>

                    <div class="linha">
                        <div class="doze colunas">
                            <label for="mensagem">Mensagem</label>
                            <textarea name="mensagem" id="mensagem" cols="10" rows="10" class="u-width-100" required></textarea>
                        </div>
                    </div>

                    <div class="linha">
                        <div class="doze colunas">
                            <input type="submit" value="Enviar" class="botao destacado">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
        include 'includes/rodape.php';
    ?>

    <script>
        $( document ).ready(function() {
            $('.nav.menu .conteudoMenu:nth-of-type(1) a:nth-of-type(2)').addClass('ativo');
        });

        function limpaCampos(){
            $("input[type='text'], textarea").val('');
        }

        $('form').submit(function (evt) {
            evt.preventDefault();

            $.ajax({
                url: "email.php",
                type: "post",
                dataType: "html",
                asyc: false,
                data: new FormData(this),
                contentType: false,
                cache: false,
                processData: false,
                success: function (result) {
                    $("#alerta").addClass("alerta-sucesso").html(result).show();
                    limpaCampos();
                    setTimeout(function(){ $("#alerta").fadeOut() }, 5000);
                },
                error: function (result) {
                    $("#alerta").addClass("alerta-erro").html(result).show();
                    limpaCampos();
                    setTimeout(function(){ $("#alerta").fadeOut() }, 5000);
                }
            });
        });
    </script>
</body>