<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<footer class="rodape">
        <div class="container">
            <div class="linha">
                <div class="duas colunas">
                    <img src="img/logo-rodape.png" alt="" width="111" height="133" class="rodape-logo">
                </div>
                <div class="tres colunas">
                    <h5 class="rodape titulo">Quem Somos</h5>
                    <a href="quem-somos.php" class="rodape texto">Institucional</a>
                    <a href="objetivos.php" class="rodape texto">Objetivos</a>
                    <a href="conselho-deliberativo.php" class="rodape texto">Conselho Deliberativo</a>
                    <a href="conselho-fiscal.php" class="rodape texto">Conselho Fiscal</a>
                    <a href="equipe.php" class="rodape texto">Equipe</a>
                </div>
                <div class="tres colunas">
                    <!-- <h5 class="rodape titulo">Notícias</h5>
                    <a href="noticias.php" class="rodape texto">Últimas Notícias</a> 
                    <h5 class="rodape titulo">Pesquisadores</h5>
                    <a href="/cgpdi_admin" class="rodape texto">Acesse sua área</a> -->
                    <h5 class="rodape titulo">Licitações</h5>
                    <a href="licitacoes.php" class="rodape texto">Últimas Licitações</a>
                    <a href="projetos.php" class="rodape texto">
                        <h5 class="rodape titulo">Portifólio</h5>
                    </a>
                </div>
                <div class="quatro colunas">
                    <h5 class="rodape titulo">Contato</h5>
                    <a href="#" class="rodape texto">cgpdi@cgpdi.org.br</a>
                    <a href="#" class="rodape texto">+55 12 3103-2504 <br />+55 12 3103-2563</a>
                    <a href="#" class="rodape texto">Rua Sete de Setembro, n300 - Centro</a>
                    <a href="#" class="rodape texto">12630-000, Cachoeira Paulista - SP</a>
                    <a href="https://goo.gl/maps/Ndv4gHchmNU2" target="_blank" class="botao botao destacado">Ver no Mapa</a>
                </div>
            </div>
        </div>
    </footer>
    <div id="direitos-autorais">
        © Copyright 2000-2018 CGPDI
    </div>

    <script src="js/jquery.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
    <script>
        window.addEventListener('DOMContentLoaded', function(e){
            document.querySelector('.nav.hamburguer')
                .addEventListener('click', function(e){
                    document.querySelector('.nav.menu').classList.toggle('aberto');
                }, false);
        }, false);

        $('nav menu').on('click', function (){
            console.log('pegou evento');
        });
    </script>
</body>
</html>