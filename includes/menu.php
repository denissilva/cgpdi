<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
</head>
<body>
    <div id="topo"></div>
    <div class="container">
    <nav class="linha nav">
            <div class="nav hamburguer">
                <img src="img/menu.png" alt="Ícone Menu" width="25" height="25">
            </div>
            <div class="tres colunas">
                <a href="index.php" class="nav logo">
                    <img src="img/logo.png" alt="" width="211" height="87">
                </a>
            </div>
            <div class="nove colunas nav menu">
                <!-- <a href="index.php" class="nav link">Home</a> -->
                <div class="conteudoMenu">
                    <a href="quem-somos.php" class="nav link">Quem Somos</a>
                    <a href="contato.php" class="nav link">Contato</a>
                    <a href="/cgpdi_admin" target="_blank" class="nav link">
                        <i class="fas fa-lock"></i>
                        Área Interna
                    </a>
                </div>
                <div class="conteudoMenu">
                    <a href="quem-somos.php" class="nav link">Institucional</a>
                    <a href="objetivos.php" class="nav link">Objetivos</a>
                    <a href="conselho-deliberativo.php" class="nav link">Conselho Deliberativo</a>
                    <a href="conselho-fiscal.php" class="nav link">Conselho Fiscal</a>
                    <a href="equipe.php" class="nav link">Equipe</a>
                </div>
            </div>
        </nav>
    </div>
</body>
</html>